/*
* 1. Odd number list
* */

var oddNumberList = function () {
    var maxVal = 100;
    var minVal = 0;

    for (var i = minVal; i <= maxVal; i++) {
        var even = i % 2;
        var odd = i != 0;


        // For even number use this:
        // if(  odd != even ){
        //        console.log(i);
        //    }

        //For odd number use this:
        if (odd = even) {
            console.log(i);
        }
    }
}
oddNumberList();


// 2. Using prompt and alert fore name

var usePrompt = function () {

    var showPrompt = prompt('What is your name?')
    var realName = 'Kakarot'
    var alertMessage = 'welcome son'

    document.write(showPrompt);
    // alert(alertMessage);

    if (showPrompt !== '' && showPrompt === realName) {
        alert(alertMessage);
    } else {
        alert('This is Kakarot?');
    }
}

// usePrompt();


// 3. Open a confirm dialog box: "Launch Nuclear arsenal - ":
// If "Yes": print message saying world is destroyed.
//     If "No": print message saying "the world is safe for now"


var saveWorld = function () {

    var checkText = 'Launch Nuclear arsenal -'
    var yesText = checkText + ' Yes'

    var confirmThis = confirm(yesText);

    if(confirmThis === true){
        console.log('world is destroyed');
    } else {
        console.log('the world is safe for now');
    }
}

// saveWorld();


// 4. Create an array with 5 shopping items, loop through the array and list all items

var shoppingItems = function () {
   var items = [ 'shoes', 'cloths', 'hats', 'mobile', 'laptop' ]
     // console.log(items);

     var itemsNumber = items.length;
     // console.log(itemsNumber);


    for(var i = 0; i < itemsNumber; i++){

        console.log(items[i]);

         // document.write(items[i] + '<br>');
    }
}

// shoppingItems();


//
// 4. Looping a triangle
// Write a loop that makes seven calls to console.log to output the following triangle:
// #
// ##
// ###
// ####
// #####
// ######
// #######

var hashList = function () {
    var hashItem = '';

    for(var i = 1; i <= 7; i++){
        hashItem += "#";
        console.log(hashItem);

        document.write(hashItem + '<br>');

    }
}
// hashList();

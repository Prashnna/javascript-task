//

//1. When should we define something as an object vs an array ?
//
//  ans: if there is a square bracts that defines array (where we can store the data on strings or with separates comma)
// and when if there is key and value that defines the object,
// we can console the object key and value with its nested keys and values.

// if we have a category of data we can make this as an object, in case we haven't a
// limited data(eg: event and day weekly and daily or monthly). If we have a unlimited data,
// we have to store we can make it an array we can use the array as an object too.
//Also if we have key and value type of data we can make it object.



//2. What is scope ? What is global scope vs local scope?
//
//  ans: Scope is a type of variable. Local scope is a variable which is only stay inside the function
// (we can use this variable only inside the function). The global scope is a variable that we can call globally
// inside other functions too or with other functions .


//3. In this sentence "I love cats, cats are mythological creatures in egypt. In the movie mummy a cat scared away the mummy"
//- count the occurrences of cats in the above sentence
//- Replace all occurrences of the word "cat" with "dog" in the following sentence "I love cats, cats are mythological creatures in egypt. In the movie mummy a cat scared away the mummy"
//

var catsCount = function () {

    //word count for cats
    var sentenseTofind = 'I love cats, cats are mythological creatures in egypt. In the movie mummy a cat scared away the mummy';
    var wordsArray = sentenseTofind.replace(',', '').split(' ');
    // console.log(wordsArray);
    var catsKey = wordsArray.indexOf('cats');
    console.log("The occurrences of 'cats' in the sentence is: " + catsKey);

    //Replace cat to dog
    var catTodog = sentenseTofind.replace(/cat/g, "dog");
    console.log(catTodog);


    //var dogArray = sentenseTofind.replace(',','').split(' ');
    //console.log(dogArray);
    //
    //var catName = 'cat';
    //var dogName = 'dog';
    //i = 0;
    //while(catName !== -1){
    //    i++;
    //    console.log(catName);
    //    sentenseTofind.replace('cats','dog');
    //    //dogArray.str.replace(catName , dogName);
    //}

    //var caTs = 'cats';
    //var removeSfromcats =  dogArray.split('cats','cat');
    //var catName = 'cat';
    //var dogName = 'dog';
    //i = 0;
    //while(catName !== -1){
    //    i++;
    //    console.log(catName);
    //    dogArray.replace('cat','dog')
    //    //dogArray.str.replace(catName , dogName);
    //}
    //console.log(dogArray);
    //var indexOfdog =  dogArray.indexOf('cat');
    //var noCatDog = indexOfdog;
    //console.log(noCatDog);
}
catsCount();


//
//4. Create a grocery list:
// - create function that adds items to grocery list
//- create function that removes items from grocery list
//- function to remove must be able to remove item by name of item e.g tomato , potato etc.
//


//Making list of Items:
var groceryItem = ['apple','chicken', 'oil'];
// console.log(groceryItem);
var addGrocery = groceryItem.push['eggs','pork','sugar'];

console.log(groceryItem);

//
//5. I have a bike:
// a. I need functionality to store bike details
//- make
//- model
//- distance travelled
//- fuel capacity
//b. Calculate mileage
//c. Given a distance to be travelled e.g. like 10km calculate if bike can travel that distance.
//    d. Create a list of problems the bike has run into and record it.
//    e. create a function that allows user to see what the make, model, distance travelled etc. of the bike is.

//Ans: 5.a


//Making list of bike details:
var bikeDetails = [
    {
        'make' : '2015',
        'modal' : 'offroad',
        'distanceTravelled' : '150km',
        'fuelCapacity': '20km/ltr'
    }
]
// console.log(bikeDetails);

bikeDetails.push({
    'make' : '2016',
    'modal' : 'onroad',
    'distanceTravelled' : '200km',
    'fuelCapacity': '25km/ltr'
});

//Making array dynamic:
function addDetails(make, modal, distanceTravelled, fuelCapacity){
    bikeDetails.push(
        {
            'make' : make,
            'modal' : modal,
            'distanceTravelled' : distanceTravelled,
            'fuelCapacity': fuelCapacity
        }
    )
}
var make = "2017",
    modal = "cruser",
    distanceTravelled = "500km",
    fuelCapacity = "40km/ltr";

addDetails(make, modal, distanceTravelled, fuelCapacity);

//looping the array
bikeDetails.forEach(function(item,index, array){
    // console.log(array);
    console.log(index + ' \nMake: ' + item.make + ' \nModal: ' + item.modal + ' \nDistance Travalled: ' + item.distanceTravelled + ' \nFuel Capacity: ' + item.fuelCapacity );
});

//
//6. Create a function that returns the sum of max and min value of a given list of numbers.
//    e.g if the numbers given were 1,2,3,4 the result would be 1+4 = 5
//    - Bonus: create a form that takes the value of multiple numbers upto 5 and returns the sum of the max and minimum number. [jQuery not allowed]